# Tasty Jaffa cogs
These cogs are simple to use, open source and can be used along side the discord bot [Red](https://github.com/Cog-Creators/Red-DiscordBot/). These are a collection of cogs I have made on various requests. These cogs are intended for use with the open source discord bot [Red](https://github.com/Cog-Creators/Red-DiscordBot/)

Programed by The Tasty Jaffa

2017-2019

There is NO WARRANTY provied with this software

You can also send a suggestions or ask for help request by sending an email to this address -> `incoming+The_Tasty_Jaffa/Tasty-Jaffa-cogs@incoming.gitlab.com`

***

# Download cogs
To download this repo use this command with your bot.

`[p]cog repo add Tasty-cogs https://github.com/The-Tasty-Jaffa/Tasty-Jaffa-cogs`


***

# The cogs

### rolenotifier
Allows you to set notification messages that will be sent to the user when a user gains that role.
This will send a custom notification message to anyone who gets that role!

`[p]cog install Tasty-cogs rolenotifier`

Once installed, run `[p]rolealert` for more information about the cog.

### tempvoice
This cog allows for the creation of temporary voice channels. These channels, once empty, will be deleted.
It also allows for admins to set a channel, which upon someone joinng, will create a new channel following a customisable format!

`[p]cog install Tasty-cogs tempvoice`

Once installed, run `[p]setvoice` for more information about the cog.


***
 
Feel free to make any git issues, or a PR. Any suggestions or support for the cogs would be apreciated! 
Oh! The Tasty Jaffa has a discord server too!
[![discord](https://discordapp.com/assets/e4923594e694a21542a489471ecffa50.svg)](https://discord.gg/MbKwDGC)
